# WTF, R?

This serves as my museum of some of the more perplexing behaviors of
[the R language].

These are the topics I've stumbled onto, but everything I've even thought of noting here is accounted for elsewhere, such as:

 * <https://github.com/ReeceGoding/Frustration-One-Year-With-R>
 * [The R Inferno by Patrick Burns](https://www.burns-stat.com/pages/Tutor/R_inferno.pdf)

[the R language]: https://www.r-project.org/
